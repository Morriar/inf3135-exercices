# Séance 6: Allocation dynamique (suite)

Dans ces exercices nous allons implémenter une chaîne de caractères à l'aide d'une
liste chaînée.

## 1 - Implémentation d'une chaîne de caractères

Complétez l'implémentation d'une structure de données de type "file" (en
anglais *queue*) à partir des déclarations suivantes:

```c
// Types
// -----

struct StringNode {
    char content;            // Contenu du noeud
    struct StringNode *next; // Noeud suivant
};

typedef struct {
    struct StringNode *first; // Pointeur vers le premier noeud
    struct StringNode *last;  // Pointeur vers le dernier noeud
} String;

// Prototypes
// ----------

// Crée une chaîne vide
String stringCreate();

// Retourne `true` si la chaîne est vide
bool stringIsEmpty(const String *s);

// Ajoute un caractère à la fin de la chaîne
void stringPush(String *s, char content);

// Retourne et retire le caractère à la fin de la chaîne
char stringPop(String *s);

// Libère l'espace mémoire alloué à la chaîne
void stringDelete(String *s);
```

Assurez-vous de gérer correctement la mémoire (pas de fuite)!

## 2 - Lecture des caractères depuis stdin

Utilisez votre implémentation pour lire un nombre indéterminé de caractères saisis
par l'utilisateur dans `stdin`.

Lorsque l'utilisateur saisit le caractère EOF, votre programme doit afficher la chaîne
complète.

Assurez-vous de gérer correctement les erreurs d'allocation en cas de mémoire insuffisante.

## 3 - Affichage de la taille de la chaîne

Implémentez la fonction `stringLength` dont voici le prototype:

```c
int stringLength(const String *s);
```

Utilisez cette conction pour afficher la taille de la chaîne saisie par l'utilisateur.

## 4 - Affichage d'un caractère de la chaîne

Implémentez la fonction `stringCharAt` dont voici le prototype:

```c
char stringCharAt(const String *s, int index);
```

Utilisez cette conction pour afficher le premier et le dernier caractère de la chaîne saisie.

En cas d'accès hors bornes (indice inférieur à 0 ou suppérieur ou égal à la taille de la chaîne),
le programme affichera une erreur avant de s'arrêter proprement.

Que pouvez-vous dire au sujet de la complexité algorithmique de cette fonction par rapport
à votre implémentation?

## 5 - Lecture à l'envers

Modifiez la structure de la chaîne pour obtenir une liste doublement chaînée de
caractères.

À l'aide de votre structure modifiée, affichez la chaîne saisie à l'endroit puis
à l'envers.

Vous ne devez pas copier la chaîne, seulement lire les maillons existants à l'envers.

## 6 - Sous-chaîne

Implémentez une fonction permettant de retourner une sous-chaîne de la chaîne saisie.

```c
String stringSub(const String *s, int from, int to);
```

La fonction doit retourner une nouvelle chaîne contenant les maillons depuis l'indice
`from` jusqu'à l'indice `to` inclus.

Aucun maillon ne doit être copié. Vous devez réutiliser les maillons de la chaîne existante.

## 7 - Copie

Implémentez une fonction permettant de retourner une copie profonde de la chaîne.
C'est à dire une copie de la structure String et de chacun des maillons qu'elle contient.

```c
String stringCopie(const String *s);
```
